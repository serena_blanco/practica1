# Obtención de una aproximación al número Pi

### El propósito de esta práctica es obtener el número Pi mediante el método de Montecarlo
---
## Pre-requisitos

Estas son las siguientes cosas que se necesitan para poder ejecutar el proyecto:
```
Versión 11 de Open JDK
```
```
Tener make instalado
```
---
## Compilación del programa
```
make jar
```
---
## Ejecutar el programa
```
make run
```
```
java -jar principal.jar <Número>
-    Ejemplo: java -jar principal.jar 2000
```
---
## Generar Javadoc
```
make javadoc
```
---
## Autores
Serena Blanco García


