/*Copyright [2021] [Serena Blanco]
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.*/


package mates;

/**
 * Clase Matematicas que contiene el método generarNumeroPi
 * @author Serena Blanco
 * @version 2.0, 27/02/2021
 */
public class Matematicas{
	
	/**
	 * Método para generar una aproximación al número Pi
	 * @param puntosTotales Total de puntos generados en el método Montecarlo
	 */
	public static void generarNumeroPi(long puntosTotales){	
		double aciertos = 0;
        	double areaCuadrado = 4;
		for (long i = 1; i <= puntosTotales; i++){
                        double x = Math.random()*2-1;
                        double y = Math.random()*2-1;
                        double distancia = Math.sqrt( Math.pow(x, 2) + Math.pow(y, 2));
			if(distancia<=1){
                                aciertos++;
                        }
                }
		
		double radio = 1;
        	double areaCirculo = areaCuadrado*(aciertos/puntosTotales);
        	double estimacionPi = areaCirculo/Math.pow(radio, 2);
		System.out.println("El número Pi es " + estimacionPi);
	}
	
}
